package models;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserStatus extends Model {

    @Id
    @GeneratedValue
    public Long id;

    @Column(unique = true, nullable = false)
    @Constraints.MaxLength(255)
    @Constraints.Required
    public String userStatusName;

    @Constraints.Required
    @Formats.DateTime(pattern = "yyyy/mm/dd")
    public Date createdAt;

    @Constraints.Required
    @Formats.DateTime(pattern = "yyyy/mm/dd")
    public Date updatedAt;

    public static Finder<Long, UserStatus> find = new Finder<Long, UserStatus>(UserStatus.class);
}
