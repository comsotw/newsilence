package models;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class UserRole extends Model {

    @Id
    @GeneratedValue
    public Long id;

    @Column(unique = true, nullable = false)
    @Constraints.MaxLength(255)
    @Constraints.Required
    public String userRoleName;

    @Constraints.Required
    @Formats.DateTime(pattern = "yyyy/mm/dd")
    public Date createdAt;

    @Constraints.Required
    @Formats.DateTime(pattern = "yyyy/mm/dd")
    public Date updatedAt;

    public static Finder<Long, UserRole> find = new Finder<Long, UserRole>(UserRole.class);
}
