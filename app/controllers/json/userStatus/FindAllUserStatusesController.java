package controllers.json.userStatus;

import akka.actor.ActorSystem;
import models.UserStatus;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import repository.userStatus.UserStatusRepository;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

@Singleton
public class FindAllUserStatusesController extends Controller {

    private final ActorSystem actorSystem;
    private final ExecutionContextExecutor exec;
    private final UserStatusRepository userStatusRepository;

    @Inject
    public FindAllUserStatusesController(
            ActorSystem actorSystem, ExecutionContextExecutor exec, UserStatusRepository userStatusRepository
    ) {
        this.actorSystem = actorSystem;
        this.exec = exec;
        this.userStatusRepository = userStatusRepository;
    }

    public CompletionStage<Result> statuses(){

        return findAll().thenApplyAsync(
                list -> ok(Json.toJson(list)), exec
        );
    }

    private CompletionStage<List<UserStatus>> findAll(){

        CompletionStage<List<UserStatus>> list = userStatusRepository.findAll();
        actorSystem.scheduler().scheduleOnce(
                Duration.create((long) 1, TimeUnit.SECONDS),
                list::toCompletableFuture,
                exec
        );

        return list;
    }
}
