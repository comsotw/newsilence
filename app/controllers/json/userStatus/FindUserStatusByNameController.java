package controllers.json.userStatus;

import akka.actor.ActorSystem;
import models.UserStatus;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import repository.userStatus.UserStatusRepository;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

@Singleton
public class FindUserStatusByNameController extends Controller {

    private final ActorSystem actorSystem;
    private final ExecutionContextExecutor exec;
    private final UserStatusRepository userStatusRepository;

    @Inject
    public FindUserStatusByNameController(
            ActorSystem actorSystem, ExecutionContextExecutor exec, UserStatusRepository userStatusRepository
    ) {
        this.actorSystem = actorSystem;
        this.exec = exec;
        this.userStatusRepository = userStatusRepository;
    }

    public CompletionStage<Result> name(String userStatusName){

        return findByName(userStatusName).thenApplyAsync(status -> ok(Json.toJson(status)), exec);
    }

    private CompletionStage<Optional<UserStatus>> findByName(String userStatusName){

        CompletionStage<Optional<UserStatus>> stage = userStatusRepository.findByUserStatusName(userStatusName);
        actorSystem.scheduler().scheduleOnce(
                Duration.create((long) 1, TimeUnit.SECONDS),
                stage::toCompletableFuture,
                exec
        );
        return stage;
    }
}
