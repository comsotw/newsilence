package controllers.json.userRole;

import akka.actor.ActorSystem;
import models.UserRole;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import repository.userRoles.UserRoleRepository;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

@Singleton
public class FindAllUserRolesController extends Controller {

    private final ActorSystem actorSystem;
    private final ExecutionContextExecutor exec;
    private final UserRoleRepository userRoleRepository;

    @Inject
    public FindAllUserRolesController(
            ActorSystem actorSystem, ExecutionContextExecutor exec, UserRoleRepository userRoleRepository
    ) {
        this.actorSystem = actorSystem;
        this.exec = exec;
        this.userRoleRepository = userRoleRepository;
    }

    public CompletionStage<Result> roles(){

        return findAll().thenApplyAsync(
                list -> ok(Json.toJson(list)), exec
        );
    }

    private CompletionStage<List<UserRole>> findAll(){

        CompletionStage<List<UserRole>> list = userRoleRepository.findAll();
        actorSystem.scheduler().scheduleOnce(
                Duration.create((long) 1, TimeUnit.SECONDS),
                list::toCompletableFuture,
                exec
        );

        return list;
    }
}
