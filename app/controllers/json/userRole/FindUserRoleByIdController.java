package controllers.json.userRole;

import akka.actor.ActorSystem;
import models.UserRole;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import repository.userRoles.UserRoleRepository;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

@Singleton
public class FindUserRoleByIdController extends Controller {

    private final ActorSystem actorSystem;
    private final ExecutionContextExecutor exec;
    private final UserRoleRepository userRoleRepository;

    @Inject
    public FindUserRoleByIdController(
            ActorSystem actorSystem, ExecutionContextExecutor exec, UserRoleRepository userRoleRepository
    ) {
        this.actorSystem = actorSystem;
        this.exec = exec;
        this.userRoleRepository = userRoleRepository;
    }

    public CompletionStage<Result> id(Long id){

        return findById(id).thenApplyAsync(role -> ok(Json.toJson(role)), exec);
    }

    private CompletionStage<Optional<UserRole>> findById(Long id){

        CompletionStage<Optional<UserRole>> stage = userRoleRepository.findById(id);
        actorSystem.scheduler().scheduleOnce(
                Duration.create((long) 1, TimeUnit.SECONDS),
                stage::toCompletableFuture,
                exec
        );
        return stage;
    }
}
