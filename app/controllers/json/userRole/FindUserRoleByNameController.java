package controllers.json.userRole;

import akka.actor.ActorSystem;
import models.UserRole;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import repository.userRoles.UserRoleRepository;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

@Singleton
public class FindUserRoleByNameController extends Controller {

    private final ActorSystem actorSystem;
    private final ExecutionContextExecutor exec;
    private final UserRoleRepository userRoleRepository;

    @Inject
    public FindUserRoleByNameController(
            ActorSystem actorSystem, ExecutionContextExecutor exec, UserRoleRepository userRoleRepository
    ) {
        this.actorSystem = actorSystem;
        this.exec = exec;
        this.userRoleRepository = userRoleRepository;
    }

    public CompletionStage<Result> name(String userStatusName){

        return findByName(userStatusName).thenApplyAsync(status -> ok(Json.toJson(status)), exec);
    }

    private CompletionStage<Optional<UserRole>> findByName(String userRoleName){

        CompletionStage<Optional<UserRole>> stage = userRoleRepository.findByName(userRoleName);
        actorSystem.scheduler().scheduleOnce(
                Duration.create((long) 1, TimeUnit.SECONDS),
                stage::toCompletableFuture,
                exec
        );
        return stage;
    }
}
