package repository.userStatus;

import models.UserStatus;

import java.util.List;
import java.util.concurrent.CompletionStage;

public interface FindAllStatuses {

    CompletionStage<List<UserStatus>> findAll();
}
