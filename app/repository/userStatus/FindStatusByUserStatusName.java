package repository.userStatus;

import models.UserStatus;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

public interface FindStatusByUserStatusName {

    CompletionStage<Optional<UserStatus>> findByUserStatusName(String userStatusName);
}
