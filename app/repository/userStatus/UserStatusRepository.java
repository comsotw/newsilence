package repository.userStatus;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import models.UserStatus;
import play.db.ebean.EbeanConfig;
import repository.DatabaseExecutionContext;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class UserStatusRepository implements FindAllStatuses, FindStatusById, FindStatusByUserStatusName {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public UserStatusRepository(EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    public CompletionStage<List<UserStatus>> findAll(){

        return supplyAsync(
                () -> ebeanServer.find(UserStatus.class).findList(), executionContext
        );
    }

    public CompletionStage<Optional<UserStatus>> findById(Long id){

        return supplyAsync(
                () -> ebeanServer.find(UserStatus.class).setId(id).findOneOrEmpty(), executionContext
        );
    }

    public CompletionStage<Optional<UserStatus>> findByUserStatusName(String userStatusName){

        return supplyAsync(
                () ->
                        ebeanServer.find(UserStatus.class).where().eq("userStatusName", userStatusName)
                                .findOneOrEmpty(),
                executionContext
        );
    }
}
