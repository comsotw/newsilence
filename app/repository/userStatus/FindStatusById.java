package repository.userStatus;

import models.UserStatus;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

public interface FindStatusById {

    CompletionStage<Optional<UserStatus>> findById(Long id);
}
