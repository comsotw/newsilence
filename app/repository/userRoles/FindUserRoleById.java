package repository.userRoles;

import models.UserRole;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

public interface FindUserRoleById {

    CompletionStage<Optional<UserRole>> findById(Long id);
}
