package repository.userRoles;

import io.ebean.Ebean;
import io.ebean.EbeanServer;
import models.UserRole;
import play.db.ebean.EbeanConfig;
import repository.DatabaseExecutionContext;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class UserRoleRepository implements FindAllUserRoles, FindUserRoleById, FindUserRoleByName {

    private final EbeanServer ebeanServer;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public UserRoleRepository(
            EbeanConfig ebeanConfig, DatabaseExecutionContext executionContext
    ) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
        this.executionContext = executionContext;
    }

    @Override
    public CompletionStage<List<UserRole>> findAll() {
        return supplyAsync(
                () -> ebeanServer.find(UserRole.class).findList(), executionContext
        );
    }

    @Override
    public CompletionStage<Optional<UserRole>> findById(Long id) {
        return supplyAsync(
                () -> ebeanServer.find(UserRole.class).setId(id).findOneOrEmpty(), executionContext
        );
    }

    @Override
    public CompletionStage<Optional<UserRole>> findByName(String userRoleName) {
        return supplyAsync(
                () ->
                        ebeanServer.find(UserRole.class).where().eq("userRoleName", userRoleName)
                                .findOneOrEmpty(),
                executionContext
        );
    }
}