package repository.userRoles;

import models.UserRole;

import java.util.List;
import java.util.concurrent.CompletionStage;

public interface FindAllUserRoles {

    CompletionStage<List<UserRole>> findAll();
}
