package repository.userRoles;

import models.UserRole;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

public interface FindUserRoleByName {

    CompletionStage<Optional<UserRole>> findByName(String userRoleName);
}
