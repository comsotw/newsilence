package repository.userRoles;

import com.google.common.collect.ImmutableMap;
import models.UserRole;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.db.Database;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.WithApplication;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

public class UserRoleRepositoryTest extends WithApplication {

    private final Long userRoleId = 1L;
    private final Long adminRoleId = 2L;
    private final Long notExistsRoleId = 0L;

    private final String userRoleName = "user";
    private final String adminRoleName = "admin";
    private final String notExistsRoleName = "not exists";

    private final int expectedSize = 2;

    private Database database;

    @Before
    public void setUp(){

        this.database = Databases.inMemory(
                "mydatabase",
                ImmutableMap.of(
                        "MODE", "MYSQL"
                ),
                ImmutableMap.of(
                        "logStatements", true
                )
        );

        Evolutions.applyEvolutions(database);
    }

    @After
    public void tearDown(){

        Evolutions.cleanupEvolutions(database);
        database.shutdown();
    }

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void findAllRolesTest(){

        final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
        final CompletionStage<List<UserRole>> stage = userRoleRepository.findAll();
        await().atMost(1, SECONDS).until(
                () -> assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(list -> {

                    return list.size() == this.expectedSize;
                })
        );
    }

    @Test
    public void findRoleByUserRoleId(){

        final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
        final CompletionStage<Optional<UserRole>> stage = userRoleRepository.findById(this.userRoleId);
        await().atMost(1, SECONDS).until(
                () -> assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {

                    if(statusOptional.isPresent()){

                        return statusOptional.get().userRoleName.equals(this.userRoleName);
                    } else {

                        return statusOptional.isPresent() == true;
                    }
                })
        );
    }

    @Test
    public void findRoleByAdminRoleId(){

        final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
        final CompletionStage<Optional<UserRole>> stage = userRoleRepository.findById(this.adminRoleId);
        await().atMost(1, SECONDS).until(
                () -> assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {

                    if(statusOptional.isPresent()){

                        return statusOptional.get().userRoleName.equals(this.adminRoleName);
                    } else {

                        return statusOptional.isPresent() == true;
                    }
                })
        );
    }

    @Test
    public void findRoleByNotExistsRoleId(){

        final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
        final CompletionStage<Optional<UserRole>> stage = userRoleRepository.findById(this.notExistsRoleId);
        await().atMost(1, SECONDS).until(
                () -> assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {

                    return statusOptional.isPresent() == false;
                })
        );
    }

    @Test
    public void findRoleByUserRoleName(){

        final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
        final CompletionStage<Optional<UserRole>> stage = userRoleRepository.findByName(this.userRoleName);
        await().atMost(1, SECONDS).until(
                () -> assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {

                    if(statusOptional.isPresent()){

                        return statusOptional.get().userRoleName.equals(this.userRoleName);
                    } else {

                        return statusOptional.isPresent() == true;
                    }
                })
        );
    }

    @Test
    public void findRoleByAdminRoleName(){

        final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
        final CompletionStage<Optional<UserRole>> stage = userRoleRepository.findByName(this.adminRoleName);
        await().atMost(1, SECONDS).until(
                () -> assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {

                    if(statusOptional.isPresent()){

                        return statusOptional.get().userRoleName.equals(this.adminRoleName);
                    } else {

                        return statusOptional.isPresent() == true;
                    }
                })
        );
    }

    @Test
    public void findRoleByNotExistsRoleName(){

        final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
        final CompletionStage<Optional<UserRole>> stage = userRoleRepository.findByName(this.notExistsRoleName);
        await().atMost(1, SECONDS).until(
                () -> assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {

                    return statusOptional.isPresent() == false;
                })
        );
    }
}