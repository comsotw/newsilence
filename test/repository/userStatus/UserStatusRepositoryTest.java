package repository.userStatus;

import com.google.common.collect.ImmutableMap;
import models.UserStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.db.Database;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.WithApplication;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

public class UserStatusRepositoryTest extends WithApplication {

    private Long activeStatusId = 1L;
    private Long inactiveStatusId = 2L;
    private Long notExistsStatusId = 0L;

    private String activeStatusName = "active";
    private String inactiveStatusName = "inactive";
    private String notExistsStatusName = "not exists";

    private int expectedSize = 2;

    private Database database;

    @Before
    public void setUp(){

        this.database = Databases.inMemory(
                "mydatabase",
                ImmutableMap.of(
                        "MODE", "MYSQL"
                ),
                ImmutableMap.of(
                        "logStatements", true
                )
        );

        Evolutions.applyEvolutions(database);
    }

    @After
    public void tearDown(){

        Evolutions.cleanupEvolutions(database);
        database.shutdown();
    }

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void findStatusByActiveStatusId(){

        final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
        final CompletionStage<Optional<UserStatus>> stage = userStatusRepository.findById(this.activeStatusId);
        await().atMost(1, SECONDS).until(() ->
                assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {
                    final UserStatus userStatus = statusOptional.get();
                    return userStatus.userStatusName.equals(this.activeStatusName);
                })
        );
    }

    @Test
    public void findStatusByInactiveStatusId(){

        final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
        final CompletionStage<Optional<UserStatus>> stage = userStatusRepository.findById(this.inactiveStatusId);
        await().atMost(1, SECONDS).until(() ->
                assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {
                    final UserStatus userStatus = statusOptional.get();
                    return userStatus.userStatusName.equals(this.inactiveStatusName);
                })
        );
    }

    @Test
    public void findStatusByNotExistsStatusId(){

        final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
        final CompletionStage<Optional<UserStatus>> stage = userStatusRepository.findById(this.notExistsStatusId);
        await().atMost(1, SECONDS).until(() ->
                assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {
                    return !statusOptional.isPresent();
                })
        );
    }

    @Test
    public void findStatusByActiveStatusName(){

        final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
        final CompletionStage<Optional<UserStatus>> stage = userStatusRepository.findByUserStatusName(this.activeStatusName);
        await().atMost(1, SECONDS).until(() ->
                assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {
                    final UserStatus userStatus = statusOptional.get();
                    return userStatus.id.equals(this.activeStatusId);
                })
        );
    }

    @Test
    public void findStatusByInactiveStatusName(){

        final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
        final CompletionStage<Optional<UserStatus>> stage = userStatusRepository.findByUserStatusName(this.inactiveStatusName);
        await().atMost(1, SECONDS).until(() ->
                assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {
                    final UserStatus userStatus = statusOptional.get();
                    return userStatus.id.equals(this.inactiveStatusId);
                })
        );
    }

    @Test
    public void findStatusByNotExistsStatusName(){

        final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
        final CompletionStage<Optional<UserStatus>> stage = userStatusRepository.findByUserStatusName(this.notExistsStatusName);
        await().atMost(1, SECONDS).until(() ->
                assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(statusOptional -> {
                    return !statusOptional.isPresent();
                })
        );
    }

    @Test
    public void findAllStatuses(){

        final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
        final CompletionStage<List<UserStatus>> stage = userStatusRepository.findAll();
        await().atMost(1, SECONDS).until(() ->
                assertThat(stage.toCompletableFuture()).isCompletedWithValueMatching(list -> {
                    return list.size() == this.expectedSize;
                })
        );
    }
}