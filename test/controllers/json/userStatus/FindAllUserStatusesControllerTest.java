package controllers.json.userStatus;

import akka.actor.ActorSystem;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.mvc.Result;
import play.test.WithApplication;
import repository.userStatus.UserStatusRepository;
import scala.concurrent.ExecutionContextExecutor;

import java.util.concurrent.CompletionStage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.*;
import static play.test.Helpers.contentAsString;

public class FindAllUserStatusesControllerTest extends WithApplication {

    private String activeStatus = "active";
    private String inactiveStatus = "inactive";

    private Database database;

    @Before
    public void setUp(){

        this.database = Databases.inMemory(
                "mydatabase",
                ImmutableMap.of(
                        "MODE", "MYSQL"
                ),
                ImmutableMap.of(
                        "logStatements", true
                )
        );

        Evolutions.applyEvolutions(database);
    }

    @After
    public void tearDown(){

        Evolutions.cleanupEvolutions(database);
        database.shutdown();
    }

    @Test
    public void testFindAllStatuses(){

        final ActorSystem actorSystem = ActorSystem.create("test");
        try {
            final ExecutionContextExecutor ec = actorSystem.dispatcher();
            final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
            final FindAllUserStatusesController controller = new FindAllUserStatusesController(actorSystem, ec, userStatusRepository);
            final CompletionStage<Result> future = controller.statuses();

            // Block until the result is completed
            await().until(() ->
                    assertThat(future.toCompletableFuture())
                            .isCompletedWithValueMatching(result -> contentAsString(result).contains(this.activeStatus))
            );
            await().until(() ->
                    assertThat(future.toCompletableFuture())
                            .isCompletedWithValueMatching(result -> contentAsString(result).contains(this.inactiveStatus))
            );
        } finally {
            actorSystem.terminate();
        }
    }

}