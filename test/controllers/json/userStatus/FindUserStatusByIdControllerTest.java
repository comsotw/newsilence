package controllers.json.userStatus;

import akka.actor.ActorSystem;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.mvc.Result;
import play.test.WithApplication;
import repository.userStatus.UserStatusRepository;
import scala.concurrent.ExecutionContextExecutor;

import java.util.concurrent.CompletionStage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.*;
import static play.test.Helpers.contentAsString;

public class FindUserStatusByIdControllerTest extends WithApplication {

    private Long activeStatusId = 1L;
    private Long inactiveStatusId = 2L;

    private String activeStatusName = "active";
    private String inactiveStatusName = "inactive";

    private Database database;

    @Before
    public void setUp(){

        this.database = Databases.inMemory(
                "mydatabase",
                ImmutableMap.of(
                        "MODE", "MYSQL"
                ),
                ImmutableMap.of(
                        "logStatements", true
                )
        );

        Evolutions.applyEvolutions(database);
    }

    @After
    public void tearDown(){

        Evolutions.cleanupEvolutions(database);
        database.shutdown();
    }

    @Test
    public void testFindActiveStatusById(){

        final ActorSystem actorSystem = ActorSystem.create("test");
        try {
            final ExecutionContextExecutor ec = actorSystem.dispatcher();
            final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
            final FindUserStatusByIdController controller = new FindUserStatusByIdController(actorSystem, ec, userStatusRepository);
            final CompletionStage<Result> future = controller.id(this.activeStatusId);

            // Block until the result is completed
            await().until(() ->
                    assertThat(future.toCompletableFuture())
                            .isCompletedWithValueMatching(result -> contentAsString(result).contains(this.activeStatusName))
            );
        } finally {
            actorSystem.terminate();
        }
    }

    @Test
    public void testFindInactiveStatusById(){

        final ActorSystem actorSystem = ActorSystem.create("test");
        try {
            final ExecutionContextExecutor ec = actorSystem.dispatcher();
            final UserStatusRepository userStatusRepository = app.injector().instanceOf(UserStatusRepository.class);
            final FindUserStatusByIdController controller = new FindUserStatusByIdController(actorSystem, ec, userStatusRepository);
            final CompletionStage<Result> future = controller.id(this.inactiveStatusId);

            // Block until the result is completed
            await().until(() ->
                    assertThat(future.toCompletableFuture())
                            .isCompletedWithValueMatching(result -> contentAsString(result).contains(this.inactiveStatusName))
            );
        } finally {
            actorSystem.terminate();
        }
    }
}