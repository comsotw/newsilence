package controllers.json.userRole;

import akka.actor.ActorSystem;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.mvc.Result;
import play.test.WithApplication;
import repository.userRoles.UserRoleRepository;
import scala.concurrent.ExecutionContextExecutor;

import java.util.concurrent.CompletionStage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.*;
import static play.test.Helpers.contentAsString;

public class FindUserRoleByIdControllerTest extends WithApplication {

    private Long userRoleId = 1L;
    private Long adminRoleId = 2L;

    private String userRoleName = "user";
    private String adminRoleName = "admin";

    private Database database;

    @Before
    public void setUp(){

        this.database = Databases.inMemory(
                "mydatabase",
                ImmutableMap.of(
                        "MODE", "MYSQL"
                ),
                ImmutableMap.of(
                        "logStatements", true
                )
        );

        Evolutions.applyEvolutions(database);
    }

    @After
    public void tearDown(){

        Evolutions.cleanupEvolutions(database);
        database.shutdown();
    }

    @Test
    public void testFindAUserById(){

        final ActorSystem actorSystem = ActorSystem.create("test");
        try {
            final ExecutionContextExecutor ec = actorSystem.dispatcher();
            final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
            final FindUserRoleByIdController controller = new FindUserRoleByIdController(actorSystem, ec, userRoleRepository);
            final CompletionStage<Result> future = controller.id(this.userRoleId);

            // Block until the result is completed
            await().until(() ->
                    assertThat(future.toCompletableFuture())
                            .isCompletedWithValueMatching(result -> contentAsString(result).contains(this.userRoleName))
            );
        } finally {
            actorSystem.terminate();
        }
    }

    @Test
    public void testFindAdminById(){

        final ActorSystem actorSystem = ActorSystem.create("test");
        try {
            final ExecutionContextExecutor ec = actorSystem.dispatcher();
            final UserRoleRepository userRoleRepository = app.injector().instanceOf(UserRoleRepository.class);
            final FindUserRoleByIdController controller = new FindUserRoleByIdController(actorSystem, ec, userRoleRepository);
            final CompletionStage<Result> future = controller.id(this.adminRoleId);

            // Block until the result is completed
            await().until(() ->
                    assertThat(future.toCompletableFuture())
                            .isCompletedWithValueMatching(result -> contentAsString(result).contains(this.adminRoleName))
            );
        } finally {
            actorSystem.terminate();
        }
    }
}