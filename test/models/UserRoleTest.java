package models;

import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.test.WithApplication;

import java.util.Date;

import static org.junit.Assert.*;

public class UserRoleTest extends WithApplication {

    private final Long userRoleId = 1L;
    private final Long adminRoleId = 2L;
    private final Long newRoleId = 3L;

    private final String userRoleName = "user";
    private final String adminRoleName = "admin";
    private final String newRoleName = "newRole";

    private final Date createdAt = new Date();
    private final Date updatedAt = new Date();

    private final int expectedSize = 2;

    private Database database;

    @Before
    public void setUp(){

        this.database = Databases.inMemory(
                "mydatabase",
                ImmutableMap.of(
                        "MODE", "MYSQL"
                ),
                ImmutableMap.of(
                        "logStatements", true
                )
        );

        Evolutions.applyEvolutions(database);
    }

    @After
    public void tearDown(){

        Evolutions.cleanupEvolutions(database);
        database.shutdown();
    }

    @Test
    public void testAllRoles(){

        assertEquals(this.expectedSize, UserRole.find.all().size());
    }

    @Test
    public void testUserRoleId(){

        assertNotNull(UserRole.find.byId(this.userRoleId));
    }

    @Test
    public void testAdminRoleId(){

        assertNotNull(UserRole.find.byId(this.adminRoleId));
    }

    @Test
    public void testUserRoleName(){

        assertNotNull(UserRole.find.query().where().eq("userRoleName", this.userRoleName));
    }

    @Test
    public void testAdminRoleName(){

        assertNotNull(UserRole.find.query().where().eq("userRoleName", this.adminRoleName));
    }

    @Test
    public void testUserRoleModel(){

        UserRole userRole = new UserRole();
        userRole.id = this.newRoleId;
        userRole.userRoleName = this.newRoleName;
        userRole.createdAt = this.createdAt;
        userRole.updatedAt = this.updatedAt;

        assertEquals(this.newRoleId, userRole.id);
        assertEquals(this.newRoleName, userRole.userRoleName);
    }
}