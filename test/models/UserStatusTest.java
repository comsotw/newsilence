package models;

import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.Databases;
import play.db.evolutions.Evolutions;
import play.test.WithApplication;

import java.util.Date;

import static org.junit.Assert.*;

public class UserStatusTest extends WithApplication {

    private final Long id = 1L;
    private final String userStatusName = "active";
    private final Date createdAt = new Date();
    private final Date updatedAt = new Date();

    private final String activeStatus = "active";
    private final String inactiveStatus = "inactive";

    private final int expectedSize = 2;

    private Database database;

    @Before
    public void setUp(){

        this.database = Databases.inMemory(
                "mydatabase",
                ImmutableMap.of(
                        "MODE", "MYSQL"
                ),
                ImmutableMap.of(
                        "logStatements", true
                )
        );

        Evolutions.applyEvolutions(database);
    }

    @After
    public void tearDown(){

        Evolutions.cleanupEvolutions(database);
        database.shutdown();
    }

    @Test
    public void userStatusTest(){

        UserStatus userStatus = new UserStatus();
        userStatus.id = this.id;
        userStatus.userStatusName = this.userStatusName;
        userStatus.createdAt = this.createdAt;
        userStatus.updatedAt = this.updatedAt;

        assertEquals(this.id, userStatus.id);
        assertEquals(this.userStatusName, userStatus.userStatusName);
        assertEquals(this.createdAt, userStatus.createdAt);
        assertEquals(this.updatedAt, userStatus.updatedAt);
    }

    @Test
    public void testDatabase() {

        assertNotNull(UserStatus.find.query().where().eq("userStatusName", this.activeStatus));
        assertNotNull(UserStatus.find.query().where().eq("userStatusName", this.inactiveStatus));
        assertEquals(this.expectedSize, UserStatus.find.all().size());
    }
}