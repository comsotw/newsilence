create table user_status(
  id bigint not null auto_increment primary key,
  user_status_name varchar(255) not null unique key,
  created_at timestamp default current_timestamp,
  updated_at timestamp default current_timestamp on update current_timestamp
) character set utf8 collate utf8_polish_ci;