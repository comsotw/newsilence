# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table user_role (
  id                            bigint auto_increment not null,
  user_role_name                varchar(255) not null,
  created_at                    timestamp,
  updated_at                    timestamp,
  constraint uq_user_role_user_role_name unique (user_role_name),
  constraint pk_user_role primary key (id)
);

create table user_status (
  id                            bigint auto_increment not null,
  user_status_name              varchar(255) not null,
  created_at                    timestamp,
  updated_at                    timestamp,
  constraint uq_user_status_user_status_name unique (user_status_name),
  constraint pk_user_status primary key (id)
);


# --- !Downs

drop table if exists user_role;

drop table if exists user_status;

