# --- !Ups

insert into user_status(id, user_status_name, created_at, updated_at) values(1, 'active', now(), now());
insert into user_status(id, user_status_name, created_at, updated_at) values(2, 'inactive', now(), now());

insert into user_role(id, user_role_name, created_at, updated_at) values(1, 'user', now(), now());
insert into user_role(id, user_role_name, created_at, updated_at) values(2, 'admin', now(), now());
# --- !Downs

delete from user_status;
delete from user_role;